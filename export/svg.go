package export

import (
	"io"
	"time"

	"framagit.org/miyaaaa/lantt"
	svg "github.com/ajstarks/svgo"
)

// SVGExporter implements the Exporter interface to export as SVG files.
type SVGExporter struct {
}

// Export diagram as SVG. The exported diagram is written to the Writer w.
func (se *SVGExporter) Export(w io.Writer, data lantt.GanttDiagram, styling lantt.StyleOptions) {
	canvas := svg.New(w)

	// Background
	canvas.Start(1400, 800)
	canvas.Rect(0, 0, 1400, 800, "fill:#222")

	// Project Title
	canvas.Rect(5, 5, 100, 3*25)
	canvas.Text(10, 25, "LANTT", "font-size:20px;fill:white")

	// Calendar Week Bar
	canvas.Rect(105, 5, 500, 25, "stroke:white;stroke-width:1px")
	canvas.Text(500/2+105, 25, "CW 1", "font-size:18px;fill:white;text-anchor:middle")

	// Date Bars
	canvas.Rect(105, 30, 500, 25, "fill:black;stroke:white;stroke-width:1px")
	now := time.Now()
	for i := 0; i < 5; i++ {
		width := 500 / 5
		middle := width / 2
		initialOffset := 105
		offset := i * width
		total := initialOffset + offset + middle
		canvas.Text(total, 50, now.Format("02.01.2006"), "font-size:12px;fill:white;text-anchor:middle;dominant-baseline:ideographic")
		yyyy, mm, dd := now.Date()
		now = time.Date(yyyy, mm, dd+1, 0, 0, 0, 0, now.Location()) // Go rolls over intelligently.
		//canvas.Line(total, 5, total, 500, "stroke:white")
	}

	// Day Units
	canvas.Rect(105, 55, 500, 25, "fill:black;stroke:white;stroke-width:1px")
	units := []string{"10", "12", "14", "16"}
	for j := 0; j < 5; j++ {
		for i := 0; i < 4; i++ {
			width := 500 / 5 / 4
			middle := width / 2
			initialOffset := 105 + j*(500/5)
			offset := i * width
			total := initialOffset + offset + middle
			canvas.Text(total, 75, units[i], "font-size:10px;fill:white;text-anchor:middle;dominant-baseline:ideographic")
			//canvas.Line(total, 5, total, 500, "stroke:white")
		}
	}

	canvas.End()
}

/*

// TODO: Construct sort of a table?

type Div struct {
    row, col uint
    content string
}

*/
