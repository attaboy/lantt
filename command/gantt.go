package command

import "framagit.org/miyaaaa/lantt"

// NewAddEpic creates a new AddEpic command.
func NewAddEpic(d *lantt.GanttDiagram, title, desc string) AddEpic {
	return AddEpic{
		diagram:     d,
		title:       title,
		description: desc,
	}
}

// AddEpic adds a new epic to a diagram.
type AddEpic struct {
	diagram            *lantt.GanttDiagram
	title, description string
}

// Execute the command.
func (cnd *AddEpic) Execute() {
	epic := lantt.Epic{
		Name: lantt.Name{
			Title:       cnd.title,
			Description: cnd.description,
		},
	}
	cnd.diagram.Epics = append(cnd.diagram.Epics, epic)
}

// Undo the command.
func (cnd *AddEpic) Undo() {
	l := len(cnd.diagram.Epics)
	cnd.diagram.Epics = cnd.diagram.Epics[:l-1]
}

func (cnd AddEpic) String() string {
	return "< AddEpic \"" + cnd.title + "\" \"" + cnd.description + "\""
}
