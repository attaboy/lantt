# LANTT

A simple CLI tool to create GANTT diagrams.

## Planned Features

- [ ] Export to SVG
- [ ] Customize styling
- [ ] Customize layout
- [ ] Layout/Styling customization using config file
- [ ] Use via CLI
- [ ] Use via markup language
- [ ] Use via graphical interface
- [ ] Web Interface?
