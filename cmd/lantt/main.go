package main

import (
	"bufio"
	"fmt"
	"os"
	"os/signal"
	"time"
	"unicode"

	"framagit.org/miyaaaa/lantt"
	"framagit.org/miyaaaa/lantt/command"
	"framagit.org/miyaaaa/lantt/export"
)

func main2() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for sig := range c {
			// Handle ^C
			_ = sig
			fmt.Println("~> Goodbye! <~")
		}
	}()

	mgr := &lantt.CommandManager{}
	dgr := &lantt.GanttDiagram{
		Name: lantt.Name{
			Title:       "Test Diagram",
			Description: "This diagram is used for testing purposes.",
		},
	}

	running := true
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("> ")
	for running && scanner.Scan() {
		text := scanner.Text()
		parts := splitInput(text)
		switch parts[0] {
		case "epic":
			c := command.NewAddEpic(dgr, parts[1], parts[2])
			fmt.Println(c)
			mgr.ExecuteCommand(&c)
		case "undo":
			mgr.Undo()
		case "exit":
			running = false
		case "print":
			fmt.Printf("%v\n", dgr)
		default:
			fmt.Println("Unknown Command!")
		}
		fmt.Print("> ")
	}

	fmt.Println("~> Goodbye! <~")
}

// splitInput splits input using unicode.IsSpace. Quoted strings are kept as
// a single string.
func splitInput(s string) []string {
	var parts []string
	part := ""
	isQuotedDouble := false
	isQuotedSingle := false

	for _, v := range s {
		if v == '"' {
			isQuotedDouble = !isQuotedDouble
		} else if v == '\'' {
			isQuotedSingle = !isQuotedSingle
		} else if !isQuotedDouble && !isQuotedSingle && unicode.IsSpace(v) {
			parts = append(parts, part)
			part = ""
		} else {
			part += string(v)
		}
	}
	parts = append(parts, part)
	return parts
}

func main() {
	fd, err := os.Create("test.svg")
	if err != nil {
		panic(err)
	}

	gantt := lantt.GanttDiagram{
		Name: lantt.Name{
			Title:       "Hello, World",
			Description: "This is a test diagram.",
		},
		Timeline: lantt.Timeline{
			Divisions:         4,
			ShowCalendarWeeks: true,
			DaysOfWeek:        lantt.Workdays,
			DateFormat:        time.RFC3339,
		},
		Epics: []lantt.Epic{
			{
				Name: lantt.Name{
					Title:       "Back-end",
					Description: "Design and Implement the Back-end",
				},
				Length: lantt.Span{
					Start:         time.Date(2022, 4, 15, 0, 0, 0, 0, time.UTC),
					PlannedLength: 16, // Divisions are 4, so 16 / 4 are 4 days.
					ActualLength:  9,
				},
				Tasks: []lantt.Task{
					{
						Name: lantt.Name{
							Title:       "Design Model",
							Description: "Design the Data Structures",
						},
						Length: lantt.Span{
							Start:         time.Date(2022, 4, 15, 0, 0, 0, 0, time.UTC),
							PlannedLength: 4,
							ActualLength:  4,
						},
					},
					{
						Name: lantt.Name{
							Title:       "Impl. SVG Parser",
							Description: "Implement the SVG Parser",
						},
						Length: lantt.Span{
							Start:         time.Date(2022, 4, 16, 0, 0, 0, 0, time.UTC),
							PlannedLength: 12,
							ActualLength:  5,
						},
					},
				},
			},
		},
	}

	styling := lantt.StyleOptions{
		BackgroundColour: 0x000000,
		ForegroundColour: 0xAFAFAF,
		ProgressBar: lantt.ProgressBar{
			PlannedColour: 0x222222,
			ActualColour:  0x565656,
		},
	}

	exporter := export.SVGExporter{}
	exporter.Export(fd, gantt, styling)
}
