package lantt

// Command implemens a method Execute to run a command.
type Command interface {
	Execute()
}

// UndoableCommand implements an additional method Undo to revert an action.
type UndoableCommand interface {
	Command
	Undo()
}

// CommandManager manages commands.
type CommandManager struct {
	stack []UndoableCommand
}

// ExecuteCommand executes a command and adds it to the stack, if it is undoable.
func (cm *CommandManager) ExecuteCommand(cmd Command) {
	cmd.Execute()

	u, ok := cmd.(UndoableCommand)
	if ok {
		cm.stack = append(cm.stack, u)
	}
}

// Undo undoes the last action.
func (cm *CommandManager) Undo() {
	l := len(cm.stack)
	if l == 0 {
		return
	}
	cmd := cm.stack[l-1]
	cmd.Undo()

	// TODO(miya): Have a redo stack?
	cm.stack = cm.stack[:l-1]
}
