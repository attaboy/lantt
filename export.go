package lantt

import "io"

// Exporter implements a method Export needed by the export strategy.
type Exporter interface {
	// Export diagram. The exported diagram is written to the Writer w.
	Export(w io.Writer, diagram GanttDiagram, styling StyleOptions) error
}
