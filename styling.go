package lantt

// StyleOptions defines the styling options for the diagram.
type StyleOptions struct {
	BackgroundColour uint
	ForegroundColour uint
	ProgressBar
}

// ProgressBar defines the styling options used on the progress bars.
type ProgressBar struct {
	PlannedColour uint
	ActualColour  uint
}

// Text defines styling options used on text.
type Text struct {
	ForegroundColour uint
	BackgroundColour uint
	Size             uint
	Orientation
}

// Orientation defines the orientation of an object.
type Orientation uint8

// Enumeration values for the Orientation.
const (
	Up Orientation = iota + 1
	Right
	Down
	Left
)
