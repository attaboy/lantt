package lantt

import (
	"time"
)

// GanttDiagram represents a GANTT diagram.
type GanttDiagram struct {
	Name
	Timeline Timeline
	Epics    []Epic
}

// Timeline contains information about the display of the diagram's timeline.
type Timeline struct {
	Divisions         uint
	ShowCalendarWeeks bool
	DaysOfWeek        Weekday
	DateFormat        string
}

// Weekday represents a day of the week.
type Weekday uint

// Bit flag values for Weekday.
const (
	Monday Weekday = 1 << iota
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday
	Sunday
)

// Workdays refers to all work days of the week.
const Workdays = Monday | Tuesday | Wednesday | Thursday | Friday

// Epic is a main (big) tasks, divided into many sub-tasks.
type Epic struct {
	Name
	Length Span
	Tasks  []Task
}

// Task is a single task.
type Task struct {
	Name   Name
	Length Span
}

// Name contains name and description information.
type Name struct {
	Title, Description string
}

// Span contains information about start and end date.
type Span struct {
	Start                       time.Time
	PlannedLength, ActualLength uint
}
